# Spring boot CI docker demo
## Automated integration testing in docker containers

Demo of how to run integration tests for a spring-boot project and a mysql db in docker containers

I took my inspiration from this article:

https://www.digitalocean.com/community/tutorials/how-to-configure-a-continuous-integration-testing-environment-with-docker-and-docker-compose-on-ubuntu-14-04

Problem with above tutorial is that artifact that is added in Dockerfile is static. With a maven project the jar features the
projectname and version. I solved this with maven resource filtering. The main project will be built into a docker image.

Also during build an integration docker compose file is generated.
This can be run:

```
docker-compose -f docker-compose_test.yml up -d
```

and then

```
docker logs <id of test container>
```
to see the test output


```
docker wait <id of test container>
```
to get the result code

```
docker-compose -f docker-compose_test.yml down
```

TODO: 

Can these commands be automatically run by maven (and hence build systems)

Now we need the 'docker-compose build' to build the test image. Would be
nicer if the dockerfile-maven-plugin could do this. However it seems to only
read a file called 'Dockerfile'. No possibility to call a custom Dockerfile
like our 'Dockerfile_test'

Base test image on a more lightweight image than ubuntu (e.g. alpine)






