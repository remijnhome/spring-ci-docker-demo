/* 
 * Developers.nl 2018
 */
/**
 * Author:  Marc Remijn, m.remijn@developers.nl
 * Created: 4-jan-2018
 */

insert into customer
(first_name,
last_name) VALUES
('Marc', 'Remijn'),
('Rik', 'Remijn')
;