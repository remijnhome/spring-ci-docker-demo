/*
 * Developers.nl 2018
 */
package com.example.demo.controller;

import com.example.demo.model.Customer;
import com.example.demo.repos.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Marc Remijn, m.remijn@developers.nl
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {

    private CustomerRepository customerRepository;
    
    public CustomerController(@Autowired CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String listCustomers(Model model) {
        model.addAttribute("customers", customerRepository.findAll());
        return "customer";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addCustomer(Model model, @RequestParam(value = "voornaam", required = true) String voornaam, @RequestParam(value = "achternaam", required = true) String achternaam) {
        customerRepository.save(new Customer(voornaam, achternaam));
        model.addAttribute("customers", customerRepository.findAll());
        return "customer";
    }

    @RequestMapping("/remove/{id}")
    public String removeCustomer(Model model, @PathVariable long id) {
        customerRepository.delete(id);
        model.addAttribute("customers", customerRepository.findAll());
        return "customer";
    }

    @ModelAttribute("allCustomers")
    public Iterable<Customer> allCustomers() {
        return customerRepository.findAll();
    }
}
