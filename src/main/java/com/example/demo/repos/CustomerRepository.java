/*
 * Developers.nl 2018
 */
package com.example.demo.repos;

import com.example.demo.model.Customer;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marc Remijn, m.remijn@developers.nl
 */
@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);

}