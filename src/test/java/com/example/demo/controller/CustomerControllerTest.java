/*
 * Developers.nl 2018
 */
package com.example.demo.controller;

import com.example.demo.model.Customer;
import com.example.demo.repos.CustomerRepository;
import java.util.Arrays;
import java.util.List;
import static org.hamcrest.Matchers.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.mockito.BDDMockito.given;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 *
 * @author Marc Remijn, m.remijn@developers.nl
 */
@RunWith(SpringRunner.class)
@WebMvcTest(CustomerController.class)

public class CustomerControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private CustomerRepository customerRepository;

    @Test
    public void testGet() throws Exception {

        Iterable<Customer> customers = Arrays.asList(new Customer[]{new Customer("Piet", "Hein"), new Customer("Cornelisz", "Coen")});

        given(this.customerRepository.findAll()).willReturn(customers);
        this.mockMvc.perform(get("/customer").accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk()).andExpect(content().string(containsString("<td>Piet</td><td>Hein</td>"))).andExpect(content().string(containsString("<td>Cornelisz</td><td>Coen</td>")));
    }

}
