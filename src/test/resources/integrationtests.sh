#!/bin/bash
# Integration test
# Runs in integration test container
# TODO: replace with wait loop

BASE_URL=http://web:8080
RESULT=0

function wait_for_ready() {
    while true;do
        if curl -q $BASE_URL > /dev/null 2>&1; then
            break;
        else
            sleep 1;
        fi
    done
}

wait_for_ready
echo "Home page should be reachable and feature a link to 'Klanten overzicht': "
if curl -q $BASE_URL | grep '<a href="/customer">Klanten overzicht</a>'; then
  echo -n "OK"
else
  echo -n "FAIL"
  RESULT=1
fi

exit $RESULT





